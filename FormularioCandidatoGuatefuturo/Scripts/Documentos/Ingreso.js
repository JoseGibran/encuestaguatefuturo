﻿$(document).ready(function () {
    $('#fileupload').change(function (e) {
        $('#containerFU').addClass('btn btn-primary btn-circle btn-xl btn-file m-progress');
        var index = $(this).attr("data-index");
        var files = e.target.files;
        if (files.length > 0) {
            if (window.FormData !== undefined) {
                var data = new FormData();
                for (var x = 0; x < files.length; x++) {
                    data.append("file" + x, files[x]);
                }
                console.log("--------------------")
                console.log(data)
                $.ajax({
                    type: "POST",
                    url: '/Documentos/UploadFiles?id=' + index,
                    contentType: false,
                    processData: false,
                    data: data,
                    success: function (result) {
                        console.log(result);
                        $('#containerFU').removeClass();
                        $('#containerFU').addClass('btn btn-success btn-circle btn-xl btn-file');
                        $('#containerFU i').removeClass();
                        $('#containerFU i').addClass('glyphicon glyphicon-ok');

                    },
                    error: function (xhr, status, p3, p4) {

                        $('#containerFU').removeClass();
                        $('#containerFU').addClass('btn btn-danger btn-circle btn-xl btn-file');
                        $('#containerFU i').addClass('glyphicon glyphicon-remove');

                        var err = "Error " + " " + status + " " + p3 + " " + p4;
                        if (xhr.responseText && xhr.responseText[0] == "{")
                            err = JSON.parse(xhr.responseText).Message;
                            console.log(err);
                    }
                });
            } else {
                alert("Su explorador no soporta la subida de archivos");
            }
        }
    });
});

  

