﻿var listItems = [];
var claseSeleccionada = "btn btn-success";
var dia = "";
var editar = false;
var totalHoras = 0;

function getSemanalHours() {
    var horasTotales = 0;
    for (var i = 0 ; i < listItems.length; i++) {
        horasTotales += parseInt(listItems[i].horas);
    }
    return horasTotales;
}

function getItemObject(item) {
    if (item != null) {
        var divItem = $('<div/>', {
            'id': 'item' + item.dia,
            'data-dia': item.dia,
            'data-toggle': 'tooltip',
            'title': item.notas,
            'onclick': 'displayOptions(this)',
            'class': 'panel panel-success item item-' + item.color
        });

        var divSlider = $('<div/>', {
            'id': item.dia + '-slide-options',
            'class': 'slide-options'
        });
        var editTool = $('<a/>', {
            'id': 'editTask' + item.dia,
            'class': 'btn btn-sm btn-hover btn-primary',
            'onclick': 'OpenEdit(this)',
            'style': 'display: none; margin-top: 35px;',
             'data-target': item.dia
        });
        var editIcon = $('<span/>', {
            'class': 'glyphicon glyphicon-pencil'
        });
        var removeTool = $('<a/>', {
            'id': 'removeTask' + item.dia,
            'style': 'display: none; margin-top: 35px;',
            'onclick': 'RemoveItem(this)',
            'class': 'btn btn-sm btn-hover btn-danger',
            'data-target': item.dia
        });
        var removeIcon = $('<span/>', {
            'class': 'glyphicon glyphicon-remove'
        });
        var h5Titulo = $('<h5/>').text(item.titulo);
        var h6 = $('<h6/>').text('Horas de Estudio: ');
        var h4Horas = $('<h4/>').text(item.horas);
        var configTool = $('<span/>', {
            'class': 'tool glyphicon glyphicon-cog',
            'style': 'left: 65px;'
        });
        editTool.append(editIcon);
        removeTool.append(removeIcon);
        divSlider.append(editTool);
        divSlider.append(removeTool);
        divItem.append(divSlider);
        divItem.append(h5Titulo);
        divItem.append(h6);
        divItem.append(h4Horas);
        divItem.append(configTool);
        return divItem;
    }
    return null;
}

$("form").submit(function (e) {
    e.preventDefault();
    var titulo = $('#titulo').val();
    var horas = $('input[name=horas]').val();
    var notas = $('#notas').val();
    var longitud = claseSeleccionada.length;
    var index = claseSeleccionada.lastIndexOf('-');
    var background = claseSeleccionada.substring(index + 1, longitud);
    var item;
   
    if (editar) {
        item = getItem(dia);
        item.titulo = titulo;
        item.horas = horas;
        item.notas = notas;
        item.color = background;
        EditItem(item)
        editar = false;
    } else {
        item = { 'titulo': titulo, 'horas': horas, 'dia': dia, 'notas': notas, 'color': background };
        AddItem(item)
    }
    this.reset();
});

function RemoveItem(Element) {
    $('#item' + $(Element).data('target')).remove();
    $('#btn' + $(Element).data('target')).css('display', 'block');
    listItems.splice(listItems.indexOf(getItem($(Element).data('target'))), 1);
    $('#totalHoras').text(getSemanalHours());
}

function AddItem(item) {
    listItems.push(item)
    $('#' + item.dia).append(getItemObject(item));
    $('#btn' + dia).css('display', 'none');
    $('#myModal').modal('toggle');
    $('#totalHoras').text(getSemanalHours());
}
function OpenEdit(Element) {
    var item = getItem($(Element).data('target'));
    $('#myModal').modal('show');
    $('#myModal .modal-title').text(item.dia);
    dia = item.dia;
    $('#titulo').val(item.titulo);
    $('#horas').val(item.horas);
    $('#notas').val(item.notas);
    $('#colorMenu').addClass('btn btn-' + item.color);
    editar = true;
}
function EditItem(item) {
    listItems[listItems.indexOf(item)].color = item.color;
    listItems[listItems.indexOf(item)].horas = item.horas;
    listItems[listItems.indexOf(item)].notas = item.notas;
    listItems[listItems.indexOf(item)].titulo = item.titulo;
    var editedItem = getItemObject(listItems[listItems.indexOf(item)]);

    $('#item' + item.dia).replaceWith(editedItem);
    $('#myModal').modal('toggle');
    $('#totalHoras').text(getSemanalHours());
   
}
function cambiarColor(clase) {
    $('#colorMenu').removeClass().addClass(clase);
    claseSeleccionada = clase;
}
function keyUp_inputHoras(Element) {
    if ($(Element).val() > 24) {
        $('#alerta').fadeIn('slow').text('"El valor de las horas no puede ser mayor a 24"')
        $(Element).val(24);
    }
}
function openModal(day) {
    $('#myModal').modal('show');
    $('#myModal .modal-title').text(day);
    dia = day;
}
var flag = false;
function displayOptions(Element) {
    if (!flag) {
        $('#' + $(Element).data('dia') + '-slide-options').css('opacity', '0.9')
                      .css('animation-name', 'degradado')
                      .css('animation-duration', '0.3s');   
        $('#editTask' + $(Element).data('dia')).fadeIn();
        $('#removeTask' + $(Element).data('dia')).fadeIn();
        flag = true;
    } else {
        $('#' + $(Element).data('dia') + '-slide-options').css('opacity', '0')
                        .css('animation-name', 'gradado')
                        .css('animation-duration', '0.3s');
        $('#editTask' + $(Element).data('dia')).fadeOut();
        $('#removeTask' + $(Element).data('dia')).fadeOut();
        flag = false;
    }
}

function getItem(day) {
    for (var index = 0; index < listItems.length; index++) {
        if (listItems[index].dia == day) {
            return listItems[index];
        }
    }
    return null;
}
function getObjtById(id) {
    return $('#' + id);
};

function ValidarRegExStr(str) {
    return (new RegExp("[a-zA-Z]")).test(str);
};

function ValidarRegExNum(num) {
    return (new RegExp("[0-9]")).test(num);
};

function validarNomenclatura(id, event) {
    var eventData = ObtenerDatosDeEvento(id, event);
    if (eventData[1] !== 8 && eventData[1] !== 32) {
        if (!ValidarRegExStr(eventData[0]) || (eventData[1] > 96 && eventData[1] < 107))
            event.preventDefault();
    }
}


function ObtenerDatosDeEvento(id, event) {
    var data = [];
    //Valor de la tecla seleccionada [String | Char]
    data[0] = String.fromCharCode(event.keyCode);
    data[1] = event.keyCode;

    return data;
};

function Save() {
    $.ajax({
        data: { 'listItems': listItems },
        type: 'POST',
        cache: false,
        url: '/DetalleHorarios/Save',
        success: function (data) {
            if (data.result) {
                alert("Respuestas Guardadas");
            } else {
                alert("Error al Guardar");
            }
        },
        fail: function (data) {
            alert("fail: " + data);
        }
    });
}

function Load(listItems) {
    for (var index = 0; index < listItems.length; index++) {
        $('#' + listItems[index].dia).append(getItemObject(listItems[index]));
    }
}


$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();

    $('#horas').focusout(function () {
        if ($(this).val() == 0 || $(this).val() < 0) {
            $(this).val(1);
            $('#alerta').fadeIn('slow').text('"El valor de las horas no puede ser 0 o Menor "');
        } else {
            $('#alerta').fadeOut('slow');
        }
    });
});