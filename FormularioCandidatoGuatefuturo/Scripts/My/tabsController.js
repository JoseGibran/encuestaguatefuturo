﻿//Controlador de tabs
(function () {
    var app = angular.module('encuesta', []);
    app.controller("TabController", function () {
        this.tab = 1;

        this.isSet = function (checkTab) {
                return this.tab == checkTab
        };
        this.setTab = function (setTab) {
            if (isPageComplete("#page_" + this.tab)) {
                return this.tab = setTab || 0;
            }
        }
        this.goNext = function () {
            if (this.tab < 6) {
                if (isPageComplete("#page_" + this.tab)) {
                    return this.tab = this.tab + 1;
                }
            } else {
                return this.tab = 6;
            }
            
        }
        this.goPrevious = function () {
            if (this.tab > 1) {
                if (isPageComplete("#_" + this.tab)) {
                    return this.tab = this.tab - 1;
                }
            } else {
                return this.tab = 1;
            }
        }
    });
    //Verificar si se respondio todo en una pagina
    function isPageComplete(Page) {
        var isComplete = true;
       
        if ($(Page).find("input[type=checkbox]").length > 0) {
           
            if ($(Page).find("input[type=checkbox]:checked").length === 0) {
                isComplete = false;
            }
        }
        if ($(Page).find("input[type=radio]").length > 0) {
            $(Page).find("input[type=radio]").each(function (index, Element) {
                if (!$("input[name='" + Element.name + "']").is(':checked')) {
                    isComplete = false;
                }
            });
        }
        if (!isComplete) {
            $('#alertDiv').fadeIn();
        } else {
            $('#alertDiv').fadeOut();
        }
        return isComplete;
    }
})();






