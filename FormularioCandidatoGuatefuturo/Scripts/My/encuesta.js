﻿
$(document).ready(function () {
    $('div.content').eq(setPages()).append("<center><input type='submit' value='Enviar' class='btn btn-success'/></center>");
    var question2 = $('#question2');
    var question4 = $('#question4');
    var question6 = $('#question6');
    var question7 = $('#question7');
    var question10 = $('#question10');
    var question13 = $('#question13');
    var question19 = $('#question19');
    var question21 = $('#question21');
    var question30 = $('#question30');
    
    question2.remove(); question4.remove(); question6.remove(); question7.remove(); question10.remove(); question13.remove(); question19.remove(); question21.remove(); question30.remove();
 
    var respuestas = [];
    
   
    //AJAX send data to controller
    $("form").submit(function (e) {
        e.preventDefault();
        if (isCheckedAll() == false) {
            alert('Marque los campos requeridos');
            
        } else {
            $('.opcion').each(function (index, Element) {
                var respuesta = { 'idPregunta': $(Element).data('question'), 'idOpcion': $(Element).data('opcion'), 'notas': $(Element).val(), 'idUsuario': 1 };
                
                switch ($(Element).data('question')) {
                    case 14: respuesta.notas = ("Programas de Beca: " + respuesta.notas); break;
                    case 15: respuesta.notas = ("Aceptaciones de Beca: " + respuesta.notas); break;
                    case 16: respuesta.notas = ("Aceptaciones Universitarias: " + respuesta.notas); break;
                    case 17: respuesta.notas = ($(Element).val() + " = " + $("#" + $(Element).attr("id").replace("input", "number")).val()); break;
                    case 26: respuesta.notas = ("Porcentaje: " + respuesta.notas); break;
                    case 19: respuesta.notas = ("Meses: " + respuesta.notas); break;
                }

                if (respuesta.idOpcion != 0) {
                    if ($(Element).attr('type') == 'radio' || $(Element).attr('type') == 'checkbox') {
                        if ($(Element).is(':checked')) {
                            respuestas.push(respuesta);
                        }
                    }
                    if ($(Element).attr('type') == 'text') {
                        if ($(Element).val().length > 0) {
                            respuestas.push(respuesta);
                        }
                    }
                    if ($(Element).data('question') == 35) {
                        respuestas.push(respuesta);
                    }
                }
            });
            $.ajax({
                data: { 'respuestas': respuestas },
                type: 'POST',
                cache: false,
                url: 'Crear',
                success: function (data) {
                    if (data.result) {
                        alert("Respuestas Guardadas");
                    } else {
                        alert("Error al Guardar");
                    }
                    respuestas = [];
                },
                fail: function (data) {
                    alert("fail: " + data);
                }
            });

        }
    });

    $("input[id='input1-1']").click(function () {
        addQuestion($('#question1'), question2);
    });
    $("input[id='input1-2']").click(function () {
        removeQuestion($('#question2'));
    });
    
    $("input[id='input3-1']").click(function () {
        addQuestion($('#question3'), question4);
    }); 
    $("input[id='input3-2']").click(function () {
        removeQuestion(question4);
    });

    $("input[id = 'input5-1']").click(function () {
        addQuestion($('#question5'), question6);
        addQuestion($('#question5'), question7);
    });
    $("input[id='input5-2']").click(function () {
        removeQuestion(question6);
        removeQuestion(question7);
    });
  
    $("input[id = 'input9-1']").click(function () {
        addQuestion($('#question9'), question10)
    });
    $("input[id = 'input9-2']").click(function () {
        removeQuestion(question10);
    });
    $("input[id = 'input12-1']").click(function () {
        addQuestion($('#question12'), question13);
    });
    $("input[id = 'input12-2']").click(function () {
        removeQuestion(question13);
    });

    $("input[id = 'input18-1']").click(function () {
        addQuestion($('#question18'), question19);
    });
    $("input[id = 'input18-2']").click(function () {
        removeQuestion(question19);
    });
    $("input[id = 'input20-1']").click(function () {
        addQuestion($('#question20'), question21);
    });
    $("input[id = 'input20-2']").click(function () {
        removeQuestion(question21);
    });

    $("input[id = 'input29-1']").click(function () {
        removeQuestion(question30);
    });
    $("input[id = 'input29-2']").click(function () {
        addQuestion($('#question29'),question30);
    });
    
    $("#question27").ready(function () {
        $("#text27-6").text("Nombre   ");
        $("#input27-1").after('<li><span>Empresa</span></li><li><input type="text" class="opcion form-control input-ls" name="27" id="input27-2" data-question="27" data-opcion="6" /></li>');
        $("#input27-1").after('<li><span>Telefono</span></li><li><input type="number" class="opcion form-control input-ls" name="27" id="input27-3" data-question="27" data-opcion="6" onkeyup="keyup_inputTelefono(this)"/></li>');
        $("#input27-1").after('<li><span>E-mail</span></li><li><input type="text" class="opcion form-control input-ls" name="27" id="input27-4" data-question="27" data-opcion="6"  onblur="validarEmail(this)"/></li>');
    });

    $('#question17').ready(function () {
        $("#text17-6").text("");
        $("#input17-1").before("<span>Examen: </span>")
        $("#input17-1").after('<span style="margin-left: 10px;">Puntuacion: </span><input type="number" id="number17-1"  onkeyup="keyup_inputPunteo(this, event)"/>')
        $("#question17 .panel-body").append('<li><a class="btn btn-warning" id="btnAgregar" style="float:right;" onclick="addExamen(this)">agregar</a></li>');
       
    });

    var input35 = $('#input35-1');
    input35.before("<textarea id='textarea35'class='opcion' data-question='" + input35.data('question') + "' data-opcion='6' style='resize: none; width:5000px; max-width: 500px; height: 120px; border: 2px double #428BCA;' maxlength='1500'/>")
    input35.remove();
  

 
    //Agregar una Pregunta al  form 
    function addQuestion(inputFirst, inputAppend) {
        inputFirst.append(inputAppend);
        inputAppend.css('display', 'none').fadeIn('slow');
    }
    //quitar una Pregunta del from
    function removeQuestion(input) {
        input.remove();
    }
   
});

function keyup_inputPunteo(Element, e) {
    if ($(Element).val() > 100){
        $(Element).val(100);
    }
}

//Solo 8 caracteres
function keyup_inputTelefono(Element) {
    if ($(Element).val() > 100000000) {
        $(Element).val(99999999);
    }
}


//Validar EMAIL
var label = $('<label class="label label-danger" >Email invalido</label>');
function validarEmail(Element) {
    var email = $(Element).val();
   
    expr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!expr.test(email)) {
        $(Element).after(label)
    } else {
        label.remove();
    }
     
}
//Solo 3 caracteres
function keyup_inputPunteo(Element, e) {
    if ($(Element).val() > 100){
        $(Element).val(100);
    }
}

//Funcion para ordenar por paginas el contido
function setPages() {
    var lastIndex = 0;
    $('ul.question').each(function (index) {
        var divPo = parseInt(((index + 1) / 6), 10);
        var q = this;
        q.remove()
        $('div.content').eq(divPo).append(q);
        lastIndex = divPo;
    });
    return lastIndex;
}


//Agregar un input
var count = 2;
function addExamen(e) {
    var beforeInput = count - 1;
    if ($('#input17-' + beforeInput).val() != "" && $('#number17-' + beforeInput).val() != "") {
        $(e).before('<span>Examen: </span><input style="margin: 10px 0;" type="text" class="opcion" name="17" id="input17-' + count + '" data-question="17" data-opcion="6" /><span style="margin-left: 10px;">Puntuacion: </span><input type="number" id="number17-' + count + '" /></br>');
        count++;
    } else {
        alert('Rellene antes el Examen anterior')
    }
    
};
//Verifica si todo ha sido contestado
function isCheckedAll() {
    $('.question').each(function (index, Element) {
        if ($(Element).find("input[type=checkbox]").length > 0) {
            if ($(Element).find("input[type=checkbox]:checked").length === 0) {
                $(Element).append('<label class="label label-danger">Seleccione almenos una opcion</label>');
                return false;
            }
        }
    });
    return true;
}

