﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FormularioCandidatoGuatefuturo.Models;

namespace FormularioCandidatoGuatefuturo.Controllers
{
    public class DetalleHorariosController : Controller
    {
        private dbFormularioGuateFuturoEntities db = new dbFormularioGuateFuturoEntities();
        
        public ActionResult Save(List<DetalleHorario> listItems)
        {
            int usuarioID = 1;
            Horario usuarioHorario = null;
            if (db.Horario.ToList().Count > 0) {
                usuarioHorario = db.Horario.First(x => x.idAplicante == usuarioID); 
            }
            if (usuarioHorario == null)
            {
                usuarioHorario = new Horario() { idAplicante = usuarioID };
                db.Horario.Add(usuarioHorario);
                db.SaveChanges();
            }
            else {
                db.DetalleHorario.RemoveRange(db.DetalleHorario.Where(x => x.idHorario == usuarioHorario.idHorario));
            }
            if (listItems != null)
            {
                foreach (DetalleHorario dHorario in listItems)
                {
                    dHorario.idHorario = usuarioHorario.idHorario;
                    db.DetalleHorario.Add(dHorario);
                }
                db.SaveChanges();
                return Json(new { result = true });
            }
            return Json(new { result = false });
        }

        // GET: DetalleHorarios
        public ActionResult Index()
        {
            var detalleHorario = db.DetalleHorario.Include(d => d.Horario);
            return View(detalleHorario.ToList());
        }

        // GET: DetalleHorarios/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleHorario detalleHorario = db.DetalleHorario.Find(id);
            if (detalleHorario == null)
            {
                return HttpNotFound();
            }
            return View(detalleHorario);
        }

        // GET: DetalleHorarios/Create
        public ActionResult Create()
        {
            ViewBag.idHorario = new SelectList(db.Horario, "idHorario", "idHorario");
            return View();
        }

        // POST: DetalleHorarios/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "idDetalleHorario,idHorario,titulo,dia,horas,notas,color")] DetalleHorario detalleHorario)
        {
            if (ModelState.IsValid)
            {
                db.DetalleHorario.Add(detalleHorario);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.idHorario = new SelectList(db.Horario, "idHorario", "idHorario", detalleHorario.idHorario);
            return View(detalleHorario);
        }

        // GET: DetalleHorarios/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleHorario detalleHorario = db.DetalleHorario.Find(id);
            if (detalleHorario == null)
            {
                return HttpNotFound();
            }
            ViewBag.idHorario = new SelectList(db.Horario, "idHorario", "idHorario", detalleHorario.idHorario);
            return View(detalleHorario);
        }

        // POST: DetalleHorarios/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "idDetalleHorario,idHorario,titulo,dia,horas,notas,color")] DetalleHorario detalleHorario)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detalleHorario).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.idHorario = new SelectList(db.Horario, "idHorario", "idHorario", detalleHorario.idHorario);
            return View(detalleHorario);
        }

        // GET: DetalleHorarios/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleHorario detalleHorario = db.DetalleHorario.Find(id);
            if (detalleHorario == null)
            {
                return HttpNotFound();
            }
            return View(detalleHorario);
        }

        // POST: DetalleHorarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            DetalleHorario detalleHorario = db.DetalleHorario.Find(id);
            db.DetalleHorario.Remove(detalleHorario);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
