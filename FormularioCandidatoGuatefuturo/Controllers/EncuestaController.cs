﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FormularioCandidatoGuatefuturo.Models;

namespace FormularioCandidatoGuatefuturo.Controllers
{
    public class EncuestaController : Controller
    {
        private dbFormularioGuateFuturoEntities db = new dbFormularioGuateFuturoEntities();
        // GET: Encuesta
        public ActionResult Index()
        {
            ViewBag.Preguntas = db.Pregunta.ToList();
            return View();
        }
        [HttpPost]
        public ActionResult Crear(List<Respuesta> respuestas)
        {
            if (respuestas != null) {
                foreach (Respuesta respuesta in respuestas)
                {
                    if (respuesta.notas == null)
                    {
                        respuesta.notas = "";
                    }
                    try
                    {
                        int i = Convert.ToInt32(respuesta.notas);
                        respuesta.notas = "";

                    }
                    catch (FormatException e) { }
                    db.Respuesta.Add(respuesta);
                }
                //db.SaveChanges();
                return Json(new { result = true });
            }
            return Json(new {result = false});
            
        }
    }
}