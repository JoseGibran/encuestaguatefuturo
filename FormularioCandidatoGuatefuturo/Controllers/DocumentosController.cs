﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FormularioCandidatoGuatefuturo.Controllers
{
    public class DocumentosController : Controller
    {

        public string[] nombresDoc = 
            {
               "","Formulario_de_solicitud_completo","Formulario_de_solicitud_tachado",
               "Carta_recomendacion1_completa","Carta_recomendacion1_tachada",
               "Carta_recomendacion2_completa","Carta_recomendacion2_tachada",
               "Ensayo_completo","Ensayo_tachado",
               "Fotocopia_DPI_completa","Fotocopia_DPI_tachada",
               "Carta_admisión_completa","Carta_admisión_tachada",
               "Programa_estudios_completo","Programa_estudios_tachado",
               "Examen_idioma_extranjero_completo","Examen_idioma_extranjero_tachado",
               "Certificado_notas_promedio_completo","Certificado_notas_promedio_tachado",
               "Certificado_puesto_licenciatura_completo","Certificado_puesto_licenciatura_tachado",
               "CV_reciboLuz_completo","CV_reciboLuz_tachado",
               "Diplomas_titulos_educacion_superior_completos","Diplomas_titulos_educacion_superior_tachados",
               "Costos_totales_programa_completo","Costos_totales_programa_tachado",
               "Constancia_laboral_completa","Constancia_laboral_tachada",
               "Constancia_laboral_completa","Constancia_laboral_tachada",
               "Carta_fondos_propios_completa","Carta_fondos_propios_tachada",
               "Poder_completa","Poder_tachado"
            };




        // GET: Documentos
        public ActionResult Index()
        {
            ViewBag.Documents = nombresDoc;
            return View();
        }
        public ActionResult Ingreso() {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UploadFiles(int id)
        {
            
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        var fileName = Path.GetFileName(nombresDoc[id]);
                        var folder = Server.MapPath("~/UsuariosDocuments/" + User.Identity.Name);
                        if (!Directory.Exists(folder))
                        {
                            Directory.CreateDirectory(folder);
                        }
                        var path = Path.Combine(folder, fileName + ".pdf");
                        using (var fileStream = System.IO.File.Create(path))
                        {
                            await stream.CopyToAsync(fileStream);
                        }
                    }
                }
            }
            catch (Exception)
            {
                Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return Json(new {result = false });
            }

            return Json(new { result = true });
        }
		
    }
}