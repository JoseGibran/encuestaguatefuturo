CREATE DATABASE dbFormularioGuateFuturo
GO 
USE dbFormularioGuateFuturo
GO

CREATE TABLE Usuario(
	idUsuario INT IDENTITY(1,1),
	nombre VARCHAR(255),
	correo VARCHAR(255),
	PRIMARY KEY(idUsuario)
)


CREATE TABLE Pregunta(
	idPregunta INT IDENTITY(1,1),
	noPregunta INT NOT NULL,
	descripcion VARCHAR(255) NOT NULL,
	PRIMARY KEY(idPregunta)
)
CREATE TABLE Opcion(
	idOpcion INT IDENTITY(1,1),
	descripcion VARCHAR(255),
	tipo INT NOT NULL,
	PRIMARY KEY(idOpcion)
)
CREATE TABLE DetallePregunta(
	idPregunta INT NOT NULL,
	idOpcion INT NOT NULL,
	PRIMARY KEY(idPregunta, idOpcion),
	FOREIGN KEY(idPregunta) REFERENCES Pregunta(idPregunta),
	FOREIGN KEY(idOpcion) REFERENCES Opcion(idOpcion)	
)

CREATE TABLE Respuesta(
	idUsuario INT NOT NULL,
	idPregunta INT NOT NULL,
	idOpcion INT NOT NULL,
	notas VARCHAR(255),
	PRIMARY KEY(idUsuario, idPregunta, idOpcion, notas),
	FOREIGN KEY(idPregunta) REFERENCES Pregunta(idPregunta),
	FOREIGN KEY(idOpcion) REFERENCES Opcion(idOpcion),
	FOREIGN KEY(idUsuario) REFERENCES Usuario(idUsuario)
)

--Usuario de Prueba
--INSERT INTO Usuario(nombre, correo) VALUES ('Jose Gibran', 'josegibranpolonsky@outlook.es');

INSERT INTO Pregunta(noPregunta, descripcion) VALUES(1,  '�Su departamento de residencia es diferente a su departamento de origen?' )
INSERT INTO Opcion(descripcion, tipo) VALUES('SI', 1)
INSERT INTO Opcion(descripcion, tipo) VALUES('NO', 1)
INSERT INTO DetallePregunta(idPregunta, idOpcion) VALUES(1, 1)
INSERT INTO DetallePregunta(idPregunta, idOpcion) VALUES(1, 2)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES(2, 'Si respondi� que s�, �qu� le motiv� a emigrar de su departamento de origen? ')
INSERT INTO Opcion(descripcion, tipo) VALUES('Mejora econ�mica', 2)
INSERT INTO Opcion(descripcion, tipo) VALUES('Mejora educativa ', 2)
INSERT INTO Opcion(descripcion, tipo) VALUES('Razones familiares', 2)
INSERT INTO Opcion(descripcion, tipo) VALUES('otro', 4)
INSERT INTO DetallePregunta VALUES(2, 3)
INSERT INTO DetallePregunta VALUES(2, 4)
INSERT INTO DetallePregunta VALUES(2, 5)
INSERT INTO DetallePregunta VALUES(2, 6)



INSERT INTO Pregunta(noPregunta, descripcion) VALUES('3', '�Ha viajado al extranjero?')
INSERT INTO DetallePregunta VALUES (3, 1)
INSERT INTO DetallePregunta VALUES (3, 2)


INSERT INTO Pregunta(noPregunta, descripcion) VALUES('4', 'Si respondi� que s�,  �cu�l fue la raz�n principal?');
INSERT INTO Opcion(descripcion, tipo) VALUES('Acad�mico/Educativo', 1);
INSERT INTO Opcion(descripcion, tipo) VALUES('Laboral/Profesional', 1);
INSERT INTO Opcion(descripcion, tipo) VALUES('Ocio/Turismo', 1);
-- INSERT INTO Opcion(descripcion, tipo) VALUES('Otros', 4);
INSERT INTO DetallePregunta(idPregunta, idOpcion) VALUES (4, 7);
INSERT INTO DetallePregunta(idPregunta, idOpcion) VALUES (4, 8);
INSERT INTO DetallePregunta(idPregunta, idOpcion) VALUES (4, 9);
INSERT INTO DetallePregunta(idPregunta, idOpcion) VALUES (4, 6);

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('5', '�Se identifica como parte de una minor�a?');
INSERT INTO DetallePregunta VALUES (5, 1)
INSERT INTO DetallePregunta VALUES (5, 2)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('6', 'Si respondi� que s�,  �con cu�l minor�a se identifica?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Cultural', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Religiosa', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Profesional', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('G�nero', 1);
	-- INSERT INTO Opcion(descripcion, tipo) VALUES('Otros', 4);
	INSERT INTO DetallePregunta VALUES(6, 10)
	INSERT INTO DetallePregunta VALUES(6, 11)
	INSERT INTO DetallePregunta VALUES(6, 12)
	INSERT INTO DetallePregunta VALUES(6, 13)
	INSERT INTO DetallePregunta VALUES(6, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('7', 'Si se identific� como parte de una minor�a �Incide ello en su b�squeda de superaci�n profesional?');
	-- INSERT INTO Opcion(descripcion, tipo) VALUES('SI', 1)
	-- INSERT INTO Opcion(descripcion, tipo) VALUES('NO', 1)
	INSERT INTO DetallePregunta VALUES (7, 1)
	INSERT INTO DetallePregunta VALUES (7, 2)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('8', '�Ha organizado alguno de los siguientes eventos?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Conferencias acad�micas', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Conferencias profesionales', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Debates', 2);
	-- INSERT INTO Opcion(descripcion, tipo) VALUES('Otros', 4);
	INSERT INTO Opcion(descripcion, tipo) VALUES('No he organizado eventos p�blicos', 2);
	INSERT INTO DetallePregunta VALUES(8, 14)
	INSERT INTO DetallePregunta VALUES(8, 15)
	INSERT INTO DetallePregunta VALUES(8, 16)
	INSERT INTO DetallePregunta VALUES(8, 17)
	INSERT INTO DetallePregunta VALUES(8, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('9', '�Ha utilizado otros servicios de Fundaci�n Guatefuturo?');
	INSERT INTO DetallePregunta VALUES (9, 1)
	INSERT INTO DetallePregunta VALUES (9, 2)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('10' , 'Si respondi� que s�, �cu�les servicios ha utilizado?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Consejer�a Acad�mica', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Talleres de Planeaci�n de Estudios en el Exterior', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Recursos de b�squeda del portal Guatefuturo', 2);
	-- INSERT INTO Opcion(descripcion, tipo) VALUES('Otros', 4);
	INSERT INTO DetallePregunta VALUES(10, 18)
	INSERT INTO DetallePregunta VALUES(10, 19)
	INSERT INTO DetallePregunta VALUES(10, 20)
	INSERT INTO DetallePregunta VALUES(10, 6)


INSERT INTO Pregunta(noPregunta, descripcion) VALUES('11', '�Qu� otros intereses adicionales a los acad�micos tiene?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Deportes', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Artes', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Voluntariado de ayuda', 2);
	INSERT INTO DetallePregunta VALUES(11, 21)
	INSERT INTO DetallePregunta VALUES(11, 22)
	INSERT INTO DetallePregunta VALUES(11, 23)
	INSERT INTO DetallePregunta VALUES(11, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('12', '�Tuvo beca o cr�dito  durante sus estudios de licenciatura?');
	INSERT INTO DetallePregunta VALUES (12, 1)
	INSERT INTO DetallePregunta VALUES (12, 2)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('13', '�Qu� beca  fue la que obtuvo?');
	INSERT INTO DetallePregunta VALUES (13, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('14', '�A cu�ntos programas de beca para estudios de postrado aplic� recientemente previo a presentar su solicitud  al programa de Fundaci�n Guatefuturo ?');
	INSERT INTO DetallePregunta VALUES (14, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('15', '�Cu�ntos de estos  programas le aceptaron?');
INSERT INTO DetallePregunta VALUES(15, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('16', '�Cu�ntas  universidades le han dado carta de aceptaci�n al momento de aplicar al programa Cr�dito-beca de Guatefuturo?');
INSERT INTO DetallePregunta VALUES(16, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('17', '�Cu�l es la calificaci�n de los ex�menes acad�micos de ingreso solicitados en su proceso de admisi�n a su postgrado  (GRE, GMAT, TOEFL, otros)  y su calificaci�n?');
INSERT INTO DetallePregunta VALUES(17, 6)
	
INSERT INTO Pregunta(noPregunta, descripcion) VALUES('18', '�Le interesa que su  postgrado incluya un per�odo de pr�ctica profesional?');
INSERT INTO DetallePregunta VALUES (18, 1)
INSERT INTO DetallePregunta VALUES (18, 2)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('19', 'Si respondi� que s�: �De cu�ntos meses?');
INSERT INTO DetallePregunta VALUES (19, 6)
	
INSERT INTO Pregunta(noPregunta, descripcion) VALUES('20', '�Trabaj� durante sus estudios?');
INSERT INTO DetallePregunta VALUES (20, 1)
INSERT INTO DetallePregunta VALUES (20, 2)
	
INSERT INTO Pregunta(noPregunta, descripcion) VALUES('21', 'Si respondi� que s�:');		
INSERT INTO Opcion(descripcion, tipo) VALUES('A tiempo completo', 1);
INSERT INTO Opcion(descripcion, tipo) VALUES('Medio tiempo', 1);
INSERT INTO Opcion(descripcion, tipo) VALUES('Horario variable', 1);
INSERT INTO DetallePregunta VALUES (21, 24)
INSERT INTO DetallePregunta VALUES (21, 25)
INSERT INTO DetallePregunta VALUES (21, 26)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('22', '�Est� trabajando actualmente?');
INSERT INTO DetallePregunta VALUES (22, 1)
INSERT INTO DetallePregunta VALUES (22, 2)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('23', '�Cu�ntas personas han estado a su cargo en su trabajo m�s reciente?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('0', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('1-3', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('4-10', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('10 o m�s', 1);
	INSERT INTO DetallePregunta VALUES (23, 27)
	INSERT INTO DetallePregunta VALUES (23, 28)
	INSERT INTO DetallePregunta VALUES (23, 29)
	INSERT INTO DetallePregunta VALUES (23, 30)
	
INSERT INTO Pregunta(noPregunta, descripcion) VALUES('24', '�En cu�les de los siguientes sectores ha laborado?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Organismo internacional', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Academia', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Investigaci�n', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Sector privado', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Emprendimiento', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Gobierno', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('ONG', 2);
	INSERT INTO DetallePregunta VALUES (24, 31)
	INSERT INTO DetallePregunta VALUES (24, 32)
	INSERT INTO DetallePregunta VALUES (24, 33)
	INSERT INTO DetallePregunta VALUES (24, 34)
	INSERT INTO DetallePregunta VALUES (24, 35)
	INSERT INTO DetallePregunta VALUES (24, 36)
	INSERT INTO DetallePregunta VALUES (24, 37)
	INSERT INTO DetallePregunta VALUES (24, 6)
	
INSERT INTO Pregunta(noPregunta, descripcion) VALUES('25', '�En qu� sectores se ve laborando en 5 a�os?');
	INSERT INTO DetallePregunta VALUES (25, 31)
	INSERT INTO DetallePregunta VALUES (25, 32)
	INSERT INTO DetallePregunta VALUES (25, 33)
	INSERT INTO DetallePregunta VALUES (25, 34)
	INSERT INTO DetallePregunta VALUES (25, 35)
	INSERT INTO DetallePregunta VALUES (25, 36)
	INSERT INTO DetallePregunta VALUES (25, 37)
	INSERT INTO DetallePregunta VALUES (25, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('26', '�Cu�l es el incremento porcentual que espera de su salario al concluir su postgrado en el exterior?');
	INSERT INTO DetallePregunta(idPregunta, idOpcion) VALUES (26, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('27', '�Desear�a informar a un contacto profesional o laboral sobre su avance en el postgrado?');
	INSERT INTO DetallePregunta VALUES (27, 6)


INSERT INTO Pregunta(noPregunta, descripcion) VALUES('28', '�Qu� asesor�a/consejos laborales  le gustar�a recibir durante sus estudios de postgrado?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Empresas especializadas que requieren su perfil de postgrado', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Contacto con  un mentor en el pa�s', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Asesor�a laboral (p.ej.: manejo de entrevista y conocimiento de perfil psicom�trico)', 1);
	INSERT INTO DetallePregunta VALUES (28, 38)
	INSERT INTO DetallePregunta VALUES (28, 39)
	INSERT INTO DetallePregunta VALUES (28, 40)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('29', '�Es la primera vez que aplica al programa Cr�dito-beca de Guatefuturo?');
	INSERT INTO	DetallePregunta VALUES (29, 1)
	INSERT INTO	DetallePregunta VALUES (29, 2)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('30', 'Si respondi� que no �qu� le motiv� a aplicar nuevamente?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Meritocracia del programa', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('N�mero de seleccionados', 1);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Prestigio del programa', 1);
	INSERT INTO DetallePregunta VALUES (30, 41)
	INSERT INTO DetallePregunta VALUES (30, 42)
	INSERT INTO DetallePregunta VALUES (30, 43)
		
INSERT INTO Pregunta(noPregunta, descripcion) VALUES('31', '�C�mo conoci� sobre Guatefuturo  y su programa Cr�dito-beca?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Medios de comunicaci�n (Prensa, revistas, radio, TV)', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Por un amigo', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Eventos (Exposiciones,  charlas,  talleres sobre becas )', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Internet (Redes sociales, sitio web, email)', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Universidades', 2);
	-- INSERT INTO Opcion(descripcion, tipo) VALUES('Otros', 4);
	INSERT INTO DetallePregunta VALUES (31, 44)
	INSERT INTO DetallePregunta VALUES (31, 45)
	INSERT INTO DetallePregunta VALUES (31, 46)
	INSERT INTO DetallePregunta VALUES (31, 47)
	INSERT INTO DetallePregunta VALUES (31, 48)
	INSERT INTO DetallePregunta VALUES (31, 6)


INSERT INTO Pregunta(noPregunta, descripcion) VALUES('32', '�En su Universidad  de estudios de licenciatura se conoce sobre el programa Cr�dito-beca de Guatefuturo?');
	INSERT INTO DetallePregunta VALUES (32, 1)
	INSERT INTO DetallePregunta VALUES (32, 2)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('33', '�Qu� le motiv� a estudiar su postgrado en el exterior?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Mejora  laboral y salarial', 2)
	INSERT INTO Opcion(descripcion, tipo) VALUES('Ampliar mi red profesional', 2)
	INSERT INTO Opcion(descripcion, tipo) VALUES('Oportunidades de negocio', 2)
	INSERT INTO Opcion(descripcion, tipo) VALUES('Contacto con otras culturas', 2)
	INSERT INTO Opcion(descripcion, tipo) VALUES('Mayor prestigio y status', 2)
	INSERT INTO Opcion(descripcion, tipo) VALUES('Superaci�n personal', 2)
	INSERT INTO DetallePregunta VALUES (33, 49)
	INSERT INTO DetallePregunta VALUES (33, 50)
	INSERT INTO DetallePregunta VALUES (33, 51)
	INSERT INTO DetallePregunta VALUES (33, 52)
	INSERT INTO DetallePregunta VALUES (33, 53)
	INSERT INTO DetallePregunta VALUES (33, 54)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('34', '�Por qu� escoger el programa Cr�dito-beca de Guatefuturo?');
	INSERT INTO Opcion(descripcion, tipo) VALUES('Porque es un logro personal responder al compromiso de  un cr�dito-beca', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Porque agrega  valor a mi hoja de vida la referencia del programa', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Porque complementa otra beca que ya gan�', 2);
	INSERT INTO Opcion(descripcion, tipo) VALUES('Porque no tengo otras alternativas', 2);
	INSERT INTO DetallePregunta VALUES (34, 55)
	INSERT INTO DetallePregunta VALUES (34, 56)
	INSERT INTO DetallePregunta VALUES (34, 57)
	INSERT INTO DetallePregunta VALUES (34, 58)
	INSERT INTO DetallePregunta VALUES (34, 6)

INSERT INTO Pregunta(noPregunta, descripcion) VALUES('35', 'Por �ltimo,  en 300 palabras m�ximo escriba que piensa  aportar a Guatemala al concluir su postgrado');
	INSERT INTO DetallePregunta VALUES (35, 6)
